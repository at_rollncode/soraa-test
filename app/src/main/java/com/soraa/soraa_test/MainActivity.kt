package com.soraa.soraa_test

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.soraa.service.SoraaCallback
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val callback by lazy { object : SoraaCallback(){

    } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (!callback.bindService(this)) {
            throw IllegalStateException("Must work properly")
        }
        fab.setOnClickListener {
            callback.service.searchForDevices("MHcCAQEEICOPXYqAkvfSbdVIV9RQGVIVIbGB0Z/UvFv6YWaIOo95oAoGCCqGSM49AwEHoUQDQgAEJvvf8KK+zTszWjm04VbEQ+0qnu8qYUJWR+Nr7IrwSOW34sjMgge3f57ShUwx5gZ7Bay7d6O9URDEuDVHMr0tJw==",
                "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEJvvf8KK+zTszWjm04VbEQ+0qnu8qYUJWR+Nr7IrwSOW34sjMgge3f57ShUwx5gZ7Bay7d6O9URDEuDVHMr0tJw==",
                "Dl9zsQN6DJPgTqvF82CHw/HpT2cGmVYyLJQlMQX413gvhQ8v3WGd/XrNicz4HzbzStpcD09+R2YS/v3ko3XOVA==", 49042)
        }
        fab1.setOnClickListener {
            callback.service.getGroupsAndChildren()
        }
    }
}
