package com.soraa.soraa_test

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex

/**
 *
 * @author Andrey Turkovsky turkovsky.andrey@gmail.com
 * @since 2019.02.25
 */
class MainApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}